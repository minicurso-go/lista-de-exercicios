package main

import "fmt" // Biblioteca de entrada e saida de dados

/*
	Crie um Slice de inteiros e solicite ao usuário que informe o tamanho do Slice
	e os valores dos elementos. Em seguida, imprima o Slice e a soma dos valores armazenados.
*/

func main() { // Função principal do codigo
	var tamanho int // Criando uma variavel para o tamanho da lista
	var soma int    // Variavel para somar os elementos da lista

	fmt.Print("Digite o tamanho da lista: ") // Printar na tela a mensagem
	fmt.Scan(&tamanho)                       // Entrada de dados

	lista := make([]int, tamanho)

	/*
		Nas linhas 14 e 17 é a maneira de criar um arrey com o tamanho sendo uma variavel
	*/

	for i := 0; i < len(lista); i++ {
		fmt.Print("Digite o numero da posição ", i, " : ") // For para percorrer todos os elementos da lista
		fmt.Scan(&lista[i])                                // Entrada de dados na posição I da lista
		soma += lista[i]                                   // O elemento da posição I vai somar com a variavel soma e ira atribuir logo em seguida
	}

	fmt.Printf("Lista %d\nSoma: %d", lista, soma)

	/*
		Printf: É uma forma de mostrar os dados na tela de modo formatado trocando
		as variaveis depois da virgula pelos equivalente "%"
		%d: Substitui variaveis do tipo inteiro
		%f: Substitui varioveis do tipo float
		%s: Substitui variaveis do tipo string
	*/

}
