package main

import "fmt"

/*
	Crie um Array de inteiros com 10 elementos. Calcule e imprima a soma dos elementos nas posições pares do Array.
*/

func main() {
	var lista [10]int // Declara um array de 10 elementos do tipo int
	soma := 0         // Inicializa a variável soma com o valor 0

	for i := 0; i < len(lista); i++ { // Loop for para iterar sobre cada elemento do array lista
		fmt.Scan(&lista[i]) // Solicita entrada do usuário e armazena na posição i do array lista

		// Verifica se o índice atual (i) é par (ou seja, se i é divisível por 2 sem resto)
		if i%2 == 0 {
			// Se o índice atual for par, adiciona o valor do elemento atual à variável soma
			soma += lista[i]
		}
	}

	fmt.Print(soma) // Imprime a soma dos elementos que estavam em posições pares no array lista
}
