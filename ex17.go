package main

import "fmt"

/*
	Faça um programa que leia dois arrays de inteiros de tamanho n e gere um terceiro array que seja a soma dos dois primeiros.
*/

func main() {

	var tamanho int // Declara uma variável tamanho do tipo int

	fmt.Print("Digite o tamanho das listas:") // Solicita ao usuário que digite o tamanho das listas
	fmt.Scan(&tamanho)                        // Lê o tamanho inserido pelo usuário e armazena na variável tamanho

	lista_1 := make([]int, tamanho) // Cria uma slice lista_1 de inteiros com o tamanho especificado
	lista_2 := make([]int, tamanho) // Cria uma slice lista_2 de inteiros com o tamanho especificado
	soma := make([]int, tamanho)    // Cria uma slice soma de inteiros com o tamanho especificado

	// Loop for para preencher a lista_1 com valores inseridos pelo usuário
	for i := 0; i < tamanho; i++ {
		fmt.Scan(&lista_1[i]) // Solicita ao usuário que insira um número e o armazena na posição i da lista_1
	}

	// Loop for para preencher a lista_2 com valores inseridos pelo usuário
	for i := 0; i < tamanho; i++ {
		fmt.Scan(&lista_2[i]) // Solicita ao usuário que insira um número e o armazena na posição i da lista_2
	}

	// Loop for para calcular a soma dos elementos correspondentes de lista_1 e lista_2 e armazenar em soma
	for i := 0; i < len(soma); i++ {
		soma[i] = lista_1[i] + lista_2[i] // Soma os elementos correspondentes de lista_1 e lista_2 e armazena em soma
	}

	fmt.Print(soma) // Imprime a soma dos elementos correspondentes de lista_1 e lista_2

}
