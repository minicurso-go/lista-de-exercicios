package main

import "fmt"

/*
	Crie um Array de inteiros com 10 elementos. Crie um novo Slice que
	contenha apenas os elementos pares do Array original. Imprima o novo Slice.
*/

func main() {
	var lista [10]int // Declara um array de 10 elementos do tipo int
	var pares []int   // Declara uma slice vazia de inteiros chamada pares

	for i := 0; i < len(lista); i++ { // Loop for para iterar sobre cada elemento do array lista
		fmt.Scan(&lista[i]) // Solicita entrada do usuário e armazena na posição i do array lista

		// Verifica se o elemento atual da lista é par
		if lista[i]%2 == 0 {
			// Se for par, adiciona o elemento à slice pares usando append
			pares = append(pares, lista[i])
		}
	}

	fmt.Print(pares) // Imprime a slice pares que contém os números pares da lista
}
