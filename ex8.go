package main

import "fmt"

/*
	Crie um Array de floats com 6 elementos. Solicite ao usuário que informe um número
	que será adicionado em todas as posições do Array. Imprima o Array resultante.
*/

func main() {
	var lista [6]float32 // Declara uma matriz (array) de 6 elementos do tipo float32
	var numero float32   // Declara uma variável do tipo float32 chamada numero

	fmt.Scan(&numero) // Solicita entrada do usuário e armazena na variável numero

	for i := 0; i < len(lista); i++ { // Loop for para iterar sobre cada elemento da lista
		lista[i] = numero // Atribui o valor da variável numero a cada elemento da lista
	}

	fmt.Print(lista) // Imprime a lista resultante

}
