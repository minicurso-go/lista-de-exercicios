package main

import "fmt"

/*
	Escreva um programa que leia um número inteiro positivo n e gere um array com os n primeiros números primos.
*/

func main() {
	var n int   // Declara uma variável n do tipo int
	numero := 1 // Inicializa uma variável numero com o valor 1

	fmt.Scan(&n) // Solicita ao usuário que insira um número inteiro e armazena na variável n

	lista := make([]int, n) // Cria uma slice de inteiros com o tamanho especificado pelo usuário

	for i := 0; i < n; i++ { // Loop for para preencher a lista com números primos até o tamanho especificado
		contador := 0 // Inicializa uma variável contador com o valor 0

		// Loop for para verificar se o número atual é primo
		for j := 1; j <= numero; j++ {
			if numero%j == 0 { // Verifica se o número é divisível por j
				contador++ // Incrementa o contador se for divisível
			}
		}

		if contador == 2 { // Se o contador for igual a 2, indica que o número é primo
			lista[i] = numero // Adiciona o número primo à lista na posição i
		} else {
			i-- // Se o número não for primo, decrementa i para que o próximo número seja verificado na mesma posição
		}

		numero++ // Incrementa o número para verificar o próximo número
	}

	fmt.Print(lista) // Imprime a lista resultante contendo números primos

}
