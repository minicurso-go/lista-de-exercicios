package main

import "fmt"

func main() {
	// Declara um array de inteiros com 7 elementos
	var lista [7]int

	// Loop for para iterar sobre cada elemento do array
	for i := 0; i < len(lista); i++ {
		// Solicita entrada do usuário e armazena na posição i do array
		fmt.Scan(&lista[i])
	}

	// Solicita ao usuário um número para ser adicionado ao primeiro elemento do array
	fmt.Scan(&lista[0])

	// Solicita ao usuário um número para ser adicionado ao último elemento do array
	fmt.Scan(&lista[len(lista)-1])
}
