package main

import "fmt"

/*
	Crie um Array de inteiros com 5 elementos. Em seguida, crie um novo Slice que
	contenha apenas os elementos do Array que são múltiplos de 3. Imprima o novo Slice.
*/

func main() {
	var lista [5]int    // Declara um array de 5 elementos do tipo int
	var multiplos []int // Declara uma slice vazia de inteiros chamada multiplos

	for i := 0; i < len(lista); i++ { // Loop for para iterar sobre cada elemento do array lista
		fmt.Scan(&lista[i]) // Solicita entrada do usuário e armazena na posição i do array lista

		// Verifica se o elemento atual da lista é múltiplo de 3
		if lista[i]%3 == 0 {
			// Se for múltiplo de 3, adiciona o elemento à slice multiplos usando append
			multiplos = append(multiplos, lista[i])
		}
	}

	fmt.Print(multiplos) // Imprime a slice multiplos que contém os números que são múltiplos de 3

}
