package main

import "fmt"

/*
	Crie um Slice de strings com tamanho 8 e solicite ao usuário que informe um valor.
	Remova todas as ocorrências desse valor no Slice e imprima o resultado.
*/

func main() {
	lista := make([]string, 8) // Cria uma slice de strings com comprimento inicial de 8
	var palavra string         // Declara uma variável do tipo string chamada palavra

	for i := 0; i < len(lista); i++ { // Loop for para iterar sobre cada elemento da lista
		fmt.Scan(&lista[i]) // Solicita entrada do usuário e armazena na posição i da lista
	}

	fmt.Scan(&palavra) // Solicita ao usuário uma palavra para remover da lista

	for i := 0; i < len(lista); i++ { // Loop for para iterar sobre cada elemento da lista novamente
		if lista[i] == palavra { // Verifica se o elemento atual da lista é igual à palavra inserida
			lista = append(lista[:i], lista[i+1:]...) // Remove a palavra da lista usando append para concatenar as partes antes e depois da palavra
			i--                                       // Decrementa i para ajustar a iteração após a remoção do elemento
		}
	}

	fmt.Print(lista) // Imprime a lista resultante após a remoção da palavra

}
