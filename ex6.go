package main

import (
	"fmt"
)

/*
	Crie um Slice de inteiros com o tamanho 5. Em seguida, solicite ao usuário que informe
	um número inteiro. Adicione esse número ao Slice apenas se ele não estiver presente.
	Imprima o Slice resultante.
*/

func main() {

	lista := make([]int, 5) // Cria uma slice de inteiros com comprimento 5 e capacidade 5
	var numero int          // Declara uma variável do tipo int chamada numero

	for i := 0; i < 5; i++ { // Loop for para iterar 5 vezes
		fmt.Scan(&numero) // Solicita entrada do usuário e armazena na variável numero
		temp := true      // Declara uma variável booleana temp e a inicializa como true

		for j := 0; j < 5; j++ { // Loop for aninhado para percorrer a slice lista
			if numero == lista[j] { // Verifica se o número digitado está na lista
				fmt.Println("Numero contido na lista, tente novamente") // Imprime uma mensagem se o número já estiver na lista
				i--                                                     // Decrementa i para permitir que o usuário insira outro número
				temp = false                                            // Altera temp para false para sinalizar que o número já existe na lista
				break                                                   // Sai do loop for interno
			}
		}

		if temp { // Verifica se temp é true
			lista[i] = numero                 // Adiciona o número à lista na posição i
			fmt.Println("Numero adicionado!") // Imprime uma mensagem informando que o número foi adicionado com sucesso
		}
	}

	fmt.Print(lista) // Imprime a lista de números após o loop

}
