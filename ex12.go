package main

import "fmt"

/*
	Crie um Slice de inteiros com tamanho 8 e solicite ao usuário que
	informe dois índices de elementos que deverão ser trocados de posição. Imprima o Slice resultante.
*/

func main() {

	lista := make([]int, 8)    // Cria uma slice de inteiros com tamanho 8
	var indice_1, indice_2 int // Declara duas variáveis do tipo int chamadas indice_1 e indice_2

	for i := 0; i < len(lista); i++ { // Loop for para iterar sobre cada elemento da slice lista
		fmt.Scan(&lista[i]) // Solicita entrada do usuário e armazena na posição i da lista
	}

	fmt.Scan(&indice_1, &indice_2) // Solicita ao usuário dois índices para trocar os elementos da lista

	temp := lista[indice_1] // Armazena temporariamente o valor do elemento na posição indice_1

	// Troca os elementos nas posições indice_1 e indice_2 na lista
	lista[indice_1] = lista[indice_2]
	lista[indice_2] = temp

	fmt.Print(lista) // Imprime a lista resultante após a troca dos elementos

}
