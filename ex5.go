package main

import "fmt"

/*
	Crie um Array de inteiros com 10 elementos. Em seguida,
	solicite ao usuário que informe um valor e verifique se esse
	valor está presente no Array. Imprima o resultado da busca.
*/

func main() {
	var lista [10]int // Um array de tamanho 10
	var numero int    // Variavel numero para comparar todos os elementos da lista

	for i := 0; i < len(lista); i++ { // For para preencher a lista
		fmt.Printf("Digite o numero da posição %d: ", i) // Mostrar na tela um texto e a variavel "i" para mostrar a posição da lista que esta preenchendo
		fmt.Scan(&lista[i])                              // Entrar com dados na posição "i" na lista
	}

	fmt.Print("Digite o número que deseja buscar: ") // Printar um texto na tela
	fmt.Scan(&numero)                                // Entrada de dados na variavel "numero"

	temp := false // Criando uma variavel temporaria chamada "temp"
	// a variavel temp inicia como false. ao comparar os elementos do array,
	//somente se o elemento for achado o "temp" se torna true, se nao achar, permanece false

	for i := 0; i < len(lista); i++ { // For para percorrer a lista inteira
		if numero == lista[i] { // Comparar o elemento da lista com o "numero"
			temp = true // Caso ache o numero na lista troca o valor de "temp"
			break       // Comando feito para sair do loop / "for"
		} else { // Caso a primeira condição não for atendidaa entra no else
			continue // Serve para o proximo giro do loop
		}
	}

	if temp { // Caso o temp for "true" entra nessa condição
		fmt.Print("O numero existe na lista") // Printar na tela o texto
	} else { // Caso o temp for "false" entra nessa condição
		fmt.Print("O numero não existe na lista") // Printar na tela o texto
	}

}
