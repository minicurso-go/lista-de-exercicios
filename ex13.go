package main

import "fmt"

/*
	Crie um Array de floats com 10 elementos. Crie um novo Slice que contenha
	apenas os elementos do Array que são maiores que 5. Imprima o novo Slice.
*/

func main() {
	var lista [10]float32 // Declara um array de 10 elementos do tipo float32
	var maior []float32   // Declara uma slice vazia de float32 chamada maior

	for i := 0; i < len(lista); i++ { // Loop for para iterar sobre cada elemento do array lista
		fmt.Scan(&lista[i]) // Solicita entrada do usuário e armazena na posição i do array lista

		// Verifica se o elemento atual da lista é maior que 5
		if lista[i] > 5 {
			// Se for maior que 5, adiciona o elemento à slice maior usando append
			maior = append(maior, lista[i])
		}
	}

	fmt.Print(maior) // Imprime a slice maior que contém os elementos maiores que 5
}
