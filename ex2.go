package main

import "fmt" // Biblioteca de entrada e saida de dados

/*
Crie um Slice de inteiros com os valores 1, 2, 3, 4 e 5.
Remova o terceiro elemento e imprima o Slice resultante.
*/

func main() {
	var slice = []int{1, 2, 3, 4, 5} // Slice de inteiros

	slice = append(slice[:2], slice[3:]...)

	/*
		Voce ira separar a primeira parte e depois a seginda parte selecionada
		logo depois vc ira juntar a parte que vc deseja retirando o desejado
	*/

	fmt.Print(slice) // Printar na tela o slice

}
