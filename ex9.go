package main

import (
	"fmt"
	"math"
)

func main() {
	// Cria uma slice de inteiros com tamanho 10
	lista := make([]int, 10)

	// Inicializa as variáveis maior e menor para os extremos possíveis
	maior := -math.MaxInt // Inicializa maior com o menor valor possível para int
	menor := math.MaxInt  // Inicializa menor com o maior valor possível para int

	// Loop for para iterar sobre cada elemento da lista
	for i := 0; i < len(lista); i++ {
		// Solicita entrada do usuário e armazena na posição i da lista
		fmt.Scan(&lista[i])

		// Verifica se o elemento atual é menor que o menor valor encontrado até agora
		if lista[i] < menor {
			menor = lista[i] // Atualiza o valor de menor se necessário
			// Verifica se o elemento atual é maior que o maior valor encontrado até agora
		} else if lista[i] > maior {
			maior = lista[i] // Atualiza o valor de maior se necessário
		}
	}

	// Imprime o maior e o menor valor armazenados na lista
	fmt.Println(maior, menor)
}
