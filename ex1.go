package main

/*
Crie um Array de inteiros com 3 elementos e
imprima a soma dos valores armazenados no Array.
*/

import "fmt" // Biblioteca de entrada e saida de dados

func main() { // Função principal do codigo
	var array [3]int // Lista de tamanho 3
	var soma int     // variavel que vai servir para somar todos os elementos da lista

	for i := 0; i < len(array); i++ { // For para percorrer todos os elementos da lista
		fmt.Scan(&array[i]) // Entrada de dados na posição I da lista
		soma += array[i]    // O elemento da posição I vai somar com a variavel soma e ira atribuir logo em seguida
	}

	println(soma) // Printar na tela a soma de todos os elementos da lista
}
