package main

import "fmt" // Biblioteca de entrada e saida de dados

/*
	Crie um Array de floats com 4 elementos e
	calcule o produto dos valores armazenados no Array.
*/

func main() {

	x := 4
	array := make([]float32, x)

	/*
		Nas linhas 12 e 13 é a maneira de criar um arrey com o tamanho sendo uma variavel
	*/

	var produto float32 = 1 // Variavel usada para armazenar o produto

	for i := 0; i < len(array); i++ { // For feito para percorrer o arrey
		fmt.Scan(&array[i]) // Entrada de dados
		produto *= array[i]
	}

	fmt.Print(produto) // Printar na tela o produto de todos os elementos do arrey

}
