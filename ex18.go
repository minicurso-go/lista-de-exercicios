package main // Declara que este arquivo faz parte do pacote "main"

import "fmt" // Importa o pacote fmt, que fornece funcionalidades para entrada e saída de dados

func main() { // Função principal do programa
	var lista [10]int // Declara uma matriz (array) de inteiros com tamanho 10

	// Loop for para ler os 10 números inteiros inseridos pelo usuário e armazená-los na lista
	for i := 0; i < 10; i++ {
		fmt.Scan(&lista[i]) // Lê o número inteiro inserido pelo usuário e armazena na posição i da lista
	}

	flag := true // Declara uma variável booleana flag e a inicializa como verdadeira

	// Loop for para verificar se os elementos da lista estão em ordem crescente
	for i := 0; i < 10; i++ {
		// Verifica se o elemento atual é menor que o próximo elemento na lista
		if lista[i] < lista[i+1] {
			flag = true // Se for verdadeiro, mantém a flag como verdadeira
		} else {
			flag = false // Se for falso, define a flag como falsa e interrompe o loop
			break        // Interrompe o loop, pois a ordem não é crescente
		}
	}

	// Verifica o valor da flag para determinar se a lista está em ordem crescente ou não e imprime a mensagem correspondente
	if flag {
		fmt.Print("crescente") // Se a flag for verdadeira, imprime "crescente"
	} else {
		fmt.Print("decrescente") // Se a flag for falsa, imprime "decrescente"
	}
}
